package edu.gatech.seclass.project1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ASLTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	// This will test the number of sentences. The delimiters have been set and the word length as well
	// This test should return 2 as there are only two sentences but both contain words greater or equal to
	// the specified word length
	@Test
	public void testCountSentences1(){
		String input = "This is a test. How many sentences do we have here.";
		String delimiters = ":;.!?";
		int minLen = 4;

		assertEquals(2,ASL.countSentences(input, minLen, delimiters));
	}
	
	// This will test the number of sentences. The delimiters have been set and the word length as well
	// This test should return 1 because only one sentence has a word that is greater or equal to
	// the specified word length
	@Test
	public void testCountSentences2(){
		String input = "This is a test. How many sentences do we have here.";
		String delimiters = ":;.!?";
		int minLen = 5;

		assertEquals(1,ASL.countSentences(input, minLen, delimiters));
	}
	
	
	// This will test the number of sentences. The delimiters have been set and the word length as well
	// This test should return 8 as there are 8 sentences each containing words greater or equal to word length
	@Test
	public void testCountSentences3(){
		String input = "This is a test. How many sentences do we have here? That is a great question! Here are some notes:"
				+ "1.test each method 2.Test delimiters. We went to the part; I arrived and it was sunny";
		String delimiters = ":;.!?";
		int minLen = 4;

		assertEquals(8,ASL.countSentences(input, minLen, delimiters));
	}
	
	
	// This will test the number of sentences. The delimiters have been set and the word length as well
	// This test should return 2 as there are 2 sentences each containing words greater or equal to word length
	// sentences and delimiter are >= to the minLen
	@Test
	public void testCountSentences4(){
		String input = "This is a test. How many sentences do we have here? That is a great question! Here are some notes:"
				+ "1.test each method 2.Test delimiters. We went to the part; I arrived and it was sunny";
		String delimiters = ":;.!?";
		int minLen = 9;

		assertEquals(2,ASL.countSentences(input, minLen, delimiters));
	}
	
	
	//This will test if a string with only one character with spaces in between will be counted as a sentences
	//even though they are less than the set length
	@Test
	public void testCountSentences5(){
		String input = "b b b b b b b.";
		String delimiters = ":;.!?";
		int minLen = 4;

		assertEquals(0,ASL.countSentences(input, minLen, delimiters));
	}
	
	/* This test is used to check our countWords method. Each sentence is specified a word length
	 A word will be considered as a word only if the number of characters are greater or equal
	 to the min length */
	
	
	//The this test case should return 6 as there are 6 words that are greater or equal to the specified length (4)
	@Test
	public void testCountWords1(){
		String input = "This is a test. How many words do we have here.";
		String delimiters = ":;.!?";
		int length = 4;

		assertEquals(6,ASL.countWords(input,length, delimiters));
	}
	
	
	//The this test case should return 10 as there are 10 words that are greater or equal to the specified length (2)
	@Test
	public void testCountWords2(){
		String input = "This is a test. How many words do we have here.";
		String delimiters = ":;.!?";
		int length = 2;

		assertEquals(10,ASL.countWords(input,length, delimiters));
	}
	
	//The this test case should return 0 as the string is empty
	@Test
	public void testCountWords3(){
		String input = "";
		String delimiters = ":;.!?";
		int length = 4;

		assertEquals(0,ASL.countWords(input,length, delimiters));
	}
	
	//The this test case should return 0 as there are no words with greater or equal to specified length (2)
	@Test
	public void testCountWords4(){
		String input = " b b b b b b.";
		String delimiters = ":;.!?";
		int length = 2;

		assertEquals(0,ASL.countWords(input,length, delimiters));
	}
	
	//The this test case should return 2 as there are only two words with greater or equal to specified length (9)
	@Test
	public void testCountWords5(){
		String input = "This is a test. How many sentences do we have here? That is a great question! Here are some notes:"
				+ "1.test each method 2.Test delimiters. We went to the part; I arrived and it was sunny";
		String delimiters = ":;.!?";
		int length = 9;

		assertEquals(2,ASL.countWords(input,length, delimiters));
	}
	
	/*Here we will be testing the wordsPerSentence method. This method uses both of the methods that were
	 * tested earlier 
	 */
	
	
	//The this test case should return 3 as there are 6 words with length greater or equal to 4 and 2 sentences
	@Test
	public void testWordsPerSentence1(){

		String input = "This is a test. How many sentences do we have here.";
		String delimiters = ":;.!?";
		int minLen = 4;
		
		assertEquals(3, ASL.wordsPerSentence(input, delimiters, minLen));
	}
	
	//The this test case should return 1 as there are 2 words with length greater or equal to 5 and 2 sentences
	@Test
	public void testWordsPerSentence2(){

		String input = "This is a test. How many sentences do we have here.";
		String delimiters = ":;.!?";
		int minLen = 5;

		assertEquals(1, ASL.wordsPerSentence(input, delimiters, minLen));
	}
	
	
	//The this test case should return 0 as there is no string
	//Testing 0 / 0 and should output Error: Program terminated unexpectedly
	@Test(expected=ArithmeticException.class)
	public void testWordsPerSentence3(){

		String input = "";
		String delimiters = ":;.!?";
		int minLen = 4;

		ASL.wordsPerSentence(input, delimiters, minLen);
	}
	
	
	//The this test case should return 0 as there is no string
	//Testing 0 / 0 and should output Error: Program terminated unexpectedly
	@Test(expected=ArithmeticException.class)
	public void testWordsPerSentence4(){

		String input = " b b b b b b.";
		String delimiters = ":;.!?";
		int minLen = 2;

		ASL.wordsPerSentence(input, delimiters, minLen);
	}
	
	
	//This test case should return 1 as only 2 words are greater or equal to 9 and therefore will return 2 sentences
	@Test
	public void testWordsPerSentence5(){

		String input = "This is a test. How many sentences do we have here? That is a great question! Here are some notes:"
				+ "1.test each method 2.Test delimiters. We went to the part; I arrived and it was sunny";
		String delimiters = ":;.!?";
		int minLen = 9;

		assertEquals(1, ASL.wordsPerSentence(input, delimiters, minLen));
	}

}
