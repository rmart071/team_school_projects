#Requirements Document -- Team 36

##1 User Requirements

###1.1 Software Interfaces
The external libraries this system will be interacting with are the Java Libraries


###1.2 User Interfaces
The tool for the user will be a java-based command line. The user will have plenty of documentation on how to use the tool. The user will specify the file path on this command line, in which the system will then run its process and return the output in the sommand line tool. If an error has occurred, the system will produce a friendly message stating the error

###1.3 User Characteristics

The users are students from a university. They normally turn in raw text files to the professor. The students can range from people who have no technology expertise to advanced users. 


##2 System Requirements
 
###2.1 Functional Requirements
- The system shall output the average number of words per sentence
- The system shall read the sentences from a java command line
- The system shall read one file specified by user
- The system shall accept the user's specified delimiters by using -d
- The system shall accept the user's specified word length by using -l
- The system shall use the default delimiters if delimiters are not specified by the user
- The system shall use the default word length if not specified by the user
- The system should display "Error: Unable to read input file" if no permission is granted
- The system should display "Error: Unable to process input file" if file does not exist
- The system should display "Error: Program terminated unexpectedly" to catch program termination
- The system should display "Error: Ill-formed command" if arguments are invalid


####2.1.1 Default Value

- The system should accept the default values if not user specifed for word length (length >= 4)
- The system should accept the default values if not user specifed for the delimiters (":;.!?")

###2.2 Non-Functional Requirements
- The system should operate in a java-based command line
- The system should be able to process 5,000 characters per second
