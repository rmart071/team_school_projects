WC(1) 

NAME
	wc - print average length of sentences  

SYNOPSIS
	wc [-d delimeters] [-l length] filename  

DESCRIPTION  
	wc prints the average length of sentences contained in FILE. By default, a sentence is defined as a sting of characters delimited by one of the characters: . ? ! : ; <EOF>    
	
OPTIONS  

	-d List of Delimiters  
		Sets the list of delimeters that determine the end of a sentence. Valid options include any valid ascii character. This list is case sensitive.
		For example:  
		`wc -d .?abcdefABCDEF: filename`

	-l Minimum length of word    
		By default, a word is considered a string of characters surrounded by either whitespace, a sentence delimiter character, or <EOF>, with a minimum length of 4 characters. If desired use the  -l option to override the default. For example: 
		wc -l 5 filename
	
ERRORS 
	You may see the following error messages:  
	
	Error: Ill-formed command
		This indicates that the command was not specified properly. Could be an invalid minimum word length or filename.
		
	Error: Unable to read input file  
		This might indicate that you have not specified the path correctly, or that the file could not otherwise be read.  

	Error: Unable to process input file  
		There may be errors in the file, or it was of a type the program could not work with.  
		
	Error: Program terminated unexpectedly  
		This occurs for any other error. The most common reason is because the number of sentences is 0, and you cannot divide by 0.
