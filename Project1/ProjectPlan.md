
# **Project Plan -- Team 36

1 Introduction
	We will create a java command line utility to measure average sentence length, including switches for various parameters including size or word, punctuation, and what to include in the average. The target audience is university students in Lauren's class.
	
	1.1 Purpose
		The purpose is to provide students with a metric to understand the length of their sentences.  They can then use this information to improve their writing skills.

2 Process Description
	The  waterfall methodology is used for this project.  
	
	2.1 Gather and Refine Requirements
		2.1.1 Activity Description
			During this activity, the development team will gather requirements from stake holders and subject matter experts and create a requirements document. 

		2.1.2 Entrance Criteria
			Authorization of the project (assignment by professor).
		2.1.3 Exit Criteria
			Completion of the Requirements Documentation
			
	2.2 Design System
		2.2.1 Activity Description
			During this activity, the solution architect will create the design documents and pass them to the development lead. 
		2.2.2 Entrance Criteria
			Completion of the Requirements documentation.
		2.2.3 Exit Criteria
			Completion of the Design Documents. This is not a deliverable for this project.
			
	2.3 Implement
		2.3.1 Activity Description
			During this activity, the development lead will further refine the design documents and develop the code of the project and will perform basic functionality testing (Junit) for the code and document the work. The code will be integrated onto a testing platform (if available).
		2.3.2 Entrance Criteria
			Sign off by FA/SME that code is ready for implementation and deployment.
		2.3.3 Exit Criteria
			Functional code, and technical documentation ready for testing.
						
	2.4 Test
		2.4.1 Activity Description
			During this activity, the quality assurance team will run the code through a series of test cases and report the results back to the development lead and project manager. The development team will fix any bugs in the code and re-submit for testing as necessary.
		2.4.2 Entrance Criteria
			Functioning Code ready for testing.
		2.4.3 Exit Criteria
			Debugged code ready for verification.
		
	2.5 Verify
		2.5.1 Activity Description
			The functional analyst and/or SME's will confirm adherence to the requirements. Any errors will be noted and fixes (if any) will be made or documented for further future code revisions. Code base is moved to a staging platform (if available).
		2.5.2 Entrance Criteria
			Debugged code ready for verification.
		2.5.3 Exit Criteria
			Sign off by FA/SME that code is ready for deployment.
	
	
	2.6 Deploy
		2.6.1 Activity Description
			During this activity, the development team will prepare, package and deploy the package to the target audience. Documentation Lead will create the User Manual.
		2.6.2 Entrance Criteria
			Sign off by FA/SME that code is ready for deployment.
		2.6.3 Exit Criteria
			Completion of User Manual and code.
			
	2.7 Maintain
		2.7.1 Activity Description
			In this activity, the code will be maintained and modified as necessary to retain functionality and/or gather requirements for a further waterfall process.
		2.7.2 Entrance Criteria
			Completion of User Manual and code.
		2.7.3 Exit Criteria 
			Termination of support, and/or start of further waterfall process.
			
		

3 Team

	3.1 - Team members
	     	
	     	Ryan Fitzgerald (RF)
	     	Austin Click (AC)
	     	Ricardo Martinez (RM)
	     	Keason Sanvordenker (KS)
    		Lauren on Video (LV)
    		Alvin on Video (AV)
    		Professor/Graduate Assistants (PGA)
    	
	3.2 Roles 
		3.2.1 Subject Matter Expert (SME)
			Requirements are captured from the SME's. They understand both the functional requirements and any processes needed to fulfill those requirements.
		3.2.2 Functional Analyst (FA)
			Elicit clear and concise requirements from the SMEs.
		3.2.3 Solutions Architect (SA)
			Transforms the requirements created by the FA into architecture and design documents. 
		3.2.4 Development Manager (DM)
			Refines SA documentation, acts as filter and point of contact for developers.
		3.2.5 Developer (DV)
			Writes code, creates unit test case.  
		3.2.6 Quality Assurance Manager(QA)
			Assures quality of solution and adherence to requirements.
		3.2.7 Deployment (DP)
			Packages code and deploys it.
		3.2.8 Documentation/Training Lead (DL)
			Creates formal system documentation and training materials/classes/CBT for system. 
		3.2.9 Project Manager (PM)
			Assigns roles and resources, assesses and reports progress,  mitigates risk, manages finances, organizes project.
   
    
	3.3 Role Assignments
			
			Role			Lead		Other Group Members
			SME				Lauren		Lauren, Professor
			FA				Austin		Alvin, Ryan, Keason, Ricardo
			SA				Ryan	
			DM				Ryan	
			DV				Ryan		Keason, Ricardo, Austin
			QA				Ricardo		Keason, Austin, Ryan
			DP				Ryan
			DL				Austin		Keason, Ricardo, Ryan
			PM				Keason		
				
					

4 Estimates

	4.1 Effort Hours				Austin			Keason			Ricardo			Ryan
		4.1.1 Requirements			1				1				1				2			
		4.1.2 System Design			1				1				1				2
		4.1.3 Implementation		2				2				2				6						
		4.1.4 Testing				2				2				6				1
		4.1.5 Verification			1				1				1				0
		4.1.6 Deployment			4				1				1				1
		4.1.7 Maintenance			0				0				0				0
		4.1.8 Project Management	0				5				0				0
		Totals:						11				13				12				12		
		

	4.2 Lines of Source Code
		4.2.1 Functional Code    200
		4.2.2 Junit Testing      200
	
