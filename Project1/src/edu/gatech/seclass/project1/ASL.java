/**
 * 
 */
package edu.gatech.seclass.project1;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Implementation of average sentence length logic. All methods are static. 
 * @author Team36
 */
public class ASL {

	/**
	 * Count the number of words read in input
	 * @param input The text to evaluate
	 * @param minLength The minimum length of a word
	 * @param delimiters Specifies which characters can be used as a sentence delimiter 
	 */
	public static int countWords(String input, int minLength, String delimiters) {
		
		// Setup scanner on input stream to use whitespace delimiters
		Pattern pattern = Pattern.compile("["+delimiters+"\\s]+");
		Scanner sc = new Scanner(input);		
		sc.useDelimiter(pattern);

		// Count words
		int count = 0;
		while (sc.hasNext()) {
			if (sc.next().length() >= minLength) { 
				count++;
			}
		}
		
		// Close scanner
		sc.close();
		
		// return number of words
		return count;

	}
 
	/**
	 * Count the number of sentences read in input
	 * @param input The text to evaluate
	 * @param minLength The minimum length of a word
	 * @param delimiters Specifies which characters can be used as a sentence delimiter 
	 */
	public static int countSentences(String input, int minLength, String delimiters) {
		
		// Setup scanner on input stream to use sentence delimiters
		Pattern pattern = Pattern.compile("["+delimiters+"]+");
		Scanner sc = new Scanner(input);
		sc.useDelimiter(pattern);
		
		// Count sentences
		int count = 0;
		while (sc.hasNext()) {
			if (countWords(sc.next(), minLength, delimiters) > 0) {
				count++;
			}
		}
		
		// Close scanner and return number of sentences
		sc.close();
		return count;
		
	}

	/**
	 * Give the average number of words per sentences contained in input
	 * @param input The text to evaluate
	 * @param minLength The minimum length of a word
	 * @param delimiters Specifies which characters can be used as a sentence delimiter 
	 */
	public static int wordsPerSentence(String input, String delimiters, int minWordLength) throws ArithmeticException {
		int sentences = countSentences(input, minWordLength, delimiters);
		int words = countWords(input, minWordLength, delimiters);
		return Math.round( words / sentences );
	}

}
