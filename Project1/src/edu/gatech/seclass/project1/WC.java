/**
 * 
 */
package edu.gatech.seclass.project1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Holds main method and parses arguments
 * @author Team36
 */
public class WC {
	
	/** Minimum word length, initialized to default value */
	private static int minWordLength = 4;
	
	/** Sentence delimiter string, initialized to default values */
	private static String sentenceDelims = ":;.!?";
	
	/** Name of input file to read */
	private static String filename;
	
	/** String to print if there is a problem reading input file due to permissions */
	static final String processError = "Error: Unable to process input file";
	
	/** String to print if the input file is not found */
	static final String readError = "Error: Unable to read input file";
	
	/** String to print if the program terminates unexpectedly for any reason */
	static final String termError = "Error: Program terminated unexpectedly";
	
	/** String to print if the arguments are invalid */
	static final String illFormError = "Error: Ill-formed command";

	/**
	 * Main method
	 * @param args See documentation for possible arguments
	 */
	public static void main(String[] args) {
		
		// Parse, validate args
		try {
			for (int i=0; i<args.length; i++) {
				if (args[i].equals("-d") && i+1 < args.length) {
					sentenceDelims = args[++i];
				} else if (args[i].equals("-l") && i+1 < args.length) {
					minWordLength = Integer.parseInt(args[++i]);
					if ( minWordLength < 1) throw new Exception();
				} else {
					filename = args[i];
				}
			}
		} catch (Exception e) {
			System.out.println(illFormError);
			return;
		}
		
		// Check if file exists
		try {
			File f = new File(filename);
			if ( ! (f.exists() && f.isFile()) ) {
				throw new Exception();
			}
		} catch (Exception e) {
			System.out.println(readError);
			return;
		}
		
		// Read into file
		String input = null;
		try {
			input = new String(Files.readAllBytes(Paths.get(filename)));
		} catch (IOException e1) {
			System.out.println(WC.processError);
		}
		
		// Invoke ASL logic, print result.
		try {
			System.out.println(ASL.wordsPerSentence(input, sentenceDelims, minWordLength));
		} catch (Exception e) {
			System.out.println(termError);
			return;
		}
		
		
	}

}
