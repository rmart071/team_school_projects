package edu.gatech.seclass.scm.models;

import android.provider.BaseColumns;

/**
 *
 */
public class SmoothieCartContract {

    public SmoothieCartContract() {}

    public static abstract class CustomerEntry implements BaseColumns {
        public static final String TABLE_NAME = "customers";
        public static final String COLUMN_NAME_FIRST_NAME = "firstName";
        public static final String COLUMN_NAME_LAST_NAME = "lastName";
        public static final String COLUMN_NAME_UUID = "uuid";
        public static final String COLUMN_NAME_BILLING_ADDRESS = "billingAddress";
        public static final String COLUMN_NAME_EMAIL_ADDRESS = "emailAddress";
        public static final String COLUMN_NAME_GOLD_DISCOUNT_PERCENT = "goldDiscount";
        public static final String COLUMN_NAME_AVAILABLE_CREDIT = "availableCredit";
        public static final String COLUMN_NAME_AVAILABLE_CREDIT_EARN_DATE = "availableCreditEarnDate";

    }

    public static abstract class TransactionEntry implements BaseColumns {
        public static final String TABLE_NAME = "transactions";
        public static final String COLUMN_NAME_TRANSACTION_ID = "_id";
        public static final String COLUMN_NAME_CUSTOMER_UUID = "customerID";
        public static final String COLUMN_NAME_TRANSACTION_DATE = "transactionDate";
        public static final String COLUMN_NAME_GROSS_SALE_AMOUNT = "grossSaleAmount";
        public static final String COLUMN_NAME_REWARD_DISCOUNT_AMOUNT = "rewardDiscountAmount";
        public static final String COLUMN_NAME_GOLD_DISCOUNT_AMOUNT = "goldDiscountAmount";
        public static final String COLUMN_NAME_NET_SALE_AMOUNT = "netSaleAmount";
    }

}
