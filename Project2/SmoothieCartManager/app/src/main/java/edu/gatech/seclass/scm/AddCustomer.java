package edu.gatech.seclass.scm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.gatech.seclass.scm.models.Customer;

public class AddCustomer extends AppCompatActivity {

    private SmoothieCartDbHelper dbHelper = SmoothieCartDbHelper.getInstance(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        final Context context = getApplicationContext();

        // set title to "Add Customer"
        TextView title = (TextView) findViewById(R.id.aecTitle);
        title.setText(getString(R.string.titleAddCustomer));

        // save button logic - add customer
        final Button saveButton = (Button) findViewById(R.id.button_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UUID uuid = UUID.randomUUID();
                Customer customer = new Customer(
                        uuid, // uuid
                        ((EditText) findViewById(R.id.aecFirstName)).getText().toString(), // first name
                        ((EditText) findViewById(R.id.aecLastName)).getText().toString(), // last name
                        ((EditText) findViewById(R.id.aecAddress)).getText().toString(), // billingAddress
                        ((EditText) findViewById(R.id.aecEmail)).getText().toString(), // emailAddress
                        0.0, // goldDiscountPercent
                        0.0, // availableCredit
                        new Date().getTime() // availableCreditEarnDate
                );
                EditText firstName = (EditText) findViewById(R.id.aecFirstName);
                EditText lastName = (EditText) findViewById(R.id.aecLastName);
                EditText email = (EditText) findViewById(R.id.aecEmail);


                if( firstName.getText().toString().trim().equals("") ) {
                    firstName.setError("Invalid First Name!");
                }else if( lastName.getText().toString().trim().equals("") ) {
                    lastName.setError("Invalid Last Name!");
                }else if( !isEmailValid(email.getText().toString())) {
                    email.setError("Invalid Email Address!");
                }else {
                    CartManager.addCustomer(customer, context);
                    Intent result = new Intent();
                    result.putExtra(Customer.EXTRA_UUID, uuid.toString());

                    setResult(Activity.RESULT_OK, result);
                    finish();
                }


            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelper.close();
    }


    public boolean isEmailValid(String email){
        if (email.isEmpty())
            return false;

        String regExpn = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }
}
