package edu.gatech.seclass.scm.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Date;
import java.util.UUID;

/**
 *
 */
public class Customer {

    public static final String EXTRA_UUID = "edu.gatech.seclass.scm.EXTRA_UUID";
    public static final double GOLD_STATUS_MIN_PURCHASES = 500.0; // $500 in purchases in year



    private UUID uuid;
    private String firstName;
    private String lastName;
    private String billingAddress;
    private String emailAddress;
    private double goldDiscountPercent;
    private double availableCredit;
    private long availableCreditEarnDate;

    public Customer(
            UUID uuid,
            String firstName,
            String lastName,
            String billingAddress,
            String emailAddress,
            double goldDiscountPercent,
            double availableCredit,
            long availableCreditEarnDate
    ) {
        this.uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.billingAddress = billingAddress;
        this.emailAddress = emailAddress;
        this.goldDiscountPercent = goldDiscountPercent;
        this.availableCredit = availableCredit;
        this.availableCreditEarnDate = availableCreditEarnDate;
    }

    /**
     * Reviews transactions since start of calendar year to check for gold status, if not yet
     * achieved.
     * @param db Readable database with transaction table
     * @return true if customer has JUST achieved gold status, false otherwise
     */
    public boolean updateGoldStatus(SQLiteDatabase db) {
        if ( ! ( this.goldDiscountPercent > 0 ) ) {
            double sum = 0.0;

            // get start of current calendar year
            Date jan1 = new Date();
            jan1.setMonth(0);
            jan1.setDate(1);
            jan1.setHours(0);
            jan1.setMinutes(0);

            // select id from transactions where COLUMN_NAME_GROSS_SALE_AMOUNT > Jan1
            String where =
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_CUSTOMER_UUID + " = ? AND " +
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_DATE + " > ? ";
            String[] whereArgs = { this.uuid.toString(), new Double(jan1.getTime()).toString() };
            String table = SmoothieCartContract.TransactionEntry.TABLE_NAME;
            Cursor cursor = db.query(table, null, where, whereArgs, null, null, null);

            // loop through Cursor, sum
            cursor.moveToFirst();
            int ns = cursor.getColumnIndex(
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_NET_SALE_AMOUNT );
            while (!cursor.isAfterLast()) {
                sum += cursor.getDouble(ns);
                cursor.moveToNext();
            }

            // check for gold status
            if ( sum > GOLD_STATUS_MIN_PURCHASES ) {
                this.goldDiscountPercent = 0.05;
                return true;
            }
        }
        return false;
    }

    public void updateAvailableReward(double credit) {
        this.availableCredit = credit;
    }

    public UUID getUUID() {
        return uuid;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public double getGoldDiscountPercent() {
        return goldDiscountPercent;
    }

    public double getAvailableCredit() {
        return availableCredit;
    }

    public long getAvailableCreditEarnDate() {
        return availableCreditEarnDate;
    }

    public void writeToDB(SQLiteDatabase db) {
        // create row from customer data

        ContentValues values = new ContentValues();
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_UUID, this.uuid.toString());
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_FIRST_NAME, this.firstName );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_LAST_NAME, this.lastName );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_BILLING_ADDRESS, this.billingAddress );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_EMAIL_ADDRESS, this.emailAddress );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_GOLD_DISCOUNT_PERCENT, this.goldDiscountPercent );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_AVAILABLE_CREDIT, this.availableCredit );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_AVAILABLE_CREDIT_EARN_DATE, this.availableCreditEarnDate);

        // insert customer
        long newRowId = db.insert(SmoothieCartContract.CustomerEntry.TABLE_NAME, null, values);
    }

    public static Customer readFromDB(String uuidString, SQLiteDatabase db) {
        // construct, execute query
        String table = SmoothieCartContract.CustomerEntry.TABLE_NAME;
        String where = SmoothieCartContract.CustomerEntry.COLUMN_NAME_UUID + " = ? ";
        String[] whereArgs = new String[]{ uuidString };
        Cursor cursor = db.query(table, null, where, whereArgs, null, null, null, "1");

        // access 1st (and should be only!) row
        cursor.moveToFirst();

        // pull values out and construct Customer object
        UUID uuid = UUID.fromString(uuidString);
        String firstName = cursor.getString(cursor.getColumnIndexOrThrow(
                SmoothieCartContract.CustomerEntry.COLUMN_NAME_FIRST_NAME));
        String lastName = cursor.getString( cursor.getColumnIndexOrThrow(
                SmoothieCartContract.CustomerEntry.COLUMN_NAME_LAST_NAME ) );
        String billingAddress = cursor.getString( cursor.getColumnIndexOrThrow(
                SmoothieCartContract.CustomerEntry.COLUMN_NAME_BILLING_ADDRESS ) );
        String emailAddress = cursor.getString(cursor.getColumnIndexOrThrow(
                SmoothieCartContract.CustomerEntry.COLUMN_NAME_EMAIL_ADDRESS));
        double goldDiscountPercent = cursor.getDouble(cursor.getColumnIndexOrThrow(
                SmoothieCartContract.CustomerEntry.COLUMN_NAME_GOLD_DISCOUNT_PERCENT));
        double availableCredit = cursor.getDouble( cursor.getColumnIndexOrThrow(
                SmoothieCartContract.CustomerEntry.COLUMN_NAME_AVAILABLE_CREDIT ) );
        long availableCreditEarnDate = cursor.getLong( cursor.getColumnIndexOrThrow(
                SmoothieCartContract.CustomerEntry.COLUMN_NAME_AVAILABLE_CREDIT_EARN_DATE ) );

        return new Customer(uuid, firstName, lastName, billingAddress, emailAddress, goldDiscountPercent, availableCredit, availableCreditEarnDate );
    }

    public boolean updateDB(SQLiteDatabase db) {
        // New values from customer object
        ContentValues values = new ContentValues();
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_UUID, this.uuid.toString());
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_FIRST_NAME, this.firstName );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_LAST_NAME, this.lastName );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_BILLING_ADDRESS, this.billingAddress );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_EMAIL_ADDRESS, this.emailAddress );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_GOLD_DISCOUNT_PERCENT, this.goldDiscountPercent );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_AVAILABLE_CREDIT, this.availableCredit );
        values.put( SmoothieCartContract.CustomerEntry.COLUMN_NAME_AVAILABLE_CREDIT_EARN_DATE, this.availableCreditEarnDate);

        // where uuid
        String where = SmoothieCartContract.CustomerEntry.COLUMN_NAME_UUID + " = ?";
        String[] whereArgs = { this.uuid.toString() };
        // update count should be 1...
        int count = db.update( SmoothieCartContract.CustomerEntry.TABLE_NAME, values, where, whereArgs);
        return count == 1;
    }

}
