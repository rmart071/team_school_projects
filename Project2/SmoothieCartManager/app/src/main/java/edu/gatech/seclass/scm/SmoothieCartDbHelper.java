package edu.gatech.seclass.scm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import edu.gatech.seclass.scm.models.SmoothieCartContract;

/**
 *
 */
public class SmoothieCartDbHelper extends SQLiteOpenHelper {

    public static SmoothieCartDbHelper instance;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "SmoothieCart.db";

    private static final String SQL_CREATE_ENTRIES_CUSTOMERS =
            "CREATE TABLE " +
            SmoothieCartContract.CustomerEntry.TABLE_NAME + " (" +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_UUID + " TEXT PRIMARY KEY, " +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_FIRST_NAME + " TEXT, " +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_LAST_NAME + " TEXT, " +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_BILLING_ADDRESS + " TEXT, " +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_EMAIL_ADDRESS + " TEXT, " +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_GOLD_DISCOUNT_PERCENT + " REAL, " +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_AVAILABLE_CREDIT + " REAL, " +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_AVAILABLE_CREDIT_EARN_DATE + " TEXT" +
            " )";

    private static final String SQL_CREATE_ENTRIES_TRANSACTIONS =
            "CREATE TABLE " +
            SmoothieCartContract.TransactionEntry.TABLE_NAME + " (" +
            SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            SmoothieCartContract.TransactionEntry.COLUMN_NAME_CUSTOMER_UUID + " TEXT, " +
            SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_DATE + " TEXT, " +
            SmoothieCartContract.TransactionEntry.COLUMN_NAME_GROSS_SALE_AMOUNT + " REAL, " +
            SmoothieCartContract.TransactionEntry.COLUMN_NAME_REWARD_DISCOUNT_AMOUNT + " REAL, " +
            SmoothieCartContract.TransactionEntry.COLUMN_NAME_GOLD_DISCOUNT_AMOUNT + " REAL, " +
            SmoothieCartContract.TransactionEntry.COLUMN_NAME_NET_SALE_AMOUNT + " REAL" +
            " )";

    private static final String SQL_CREATE_TEST_CUSTOMERS =
            "INSERT INTO " +
            SmoothieCartContract.CustomerEntry.TABLE_NAME +
            " (" + SmoothieCartContract.CustomerEntry.COLUMN_NAME_UUID + "," +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_FIRST_NAME + "," +
            SmoothieCartContract.CustomerEntry.COLUMN_NAME_LAST_NAME + ") VALUES " +
            "( \"b53b7c86-ffee-addb-be35-2f1f4dcd8e1a\",\"Ralph\",\"Hapschatt\"), " +
            "( \"f184cd0f-0e05-6d4c-58c4-b0264e5a6bcc\",\"Everett\",\"Scott\"), " +
            "( \"b6acb594-41af-4ea1-3129-d8373df8145e\", \"Betty\", \"Monroe\")";

    private SmoothieCartDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES_CUSTOMERS);
        db.execSQL(SQL_CREATE_ENTRIES_TRANSACTIONS);
        db.execSQL(SQL_CREATE_TEST_CUSTOMERS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // out of scope
    }

    public static SmoothieCartDbHelper getInstance(Context context) {
        /*if (instance == null) {
            instance = new SmoothieCartDbHelper(context);
        }
        return instance;*/
        return new SmoothieCartDbHelper(context);
    }

}
