package edu.gatech.seclass.scm;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.gatech.seclass.scm.models.Customer;

public class EditCustomer extends AppCompatActivity {

    private SmoothieCartDbHelper dbHelper = SmoothieCartDbHelper.getInstance(this);
    protected double goldDiscountPercent;
    protected double availableCredit;
    protected long availableCreditEarnDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);

        // set title to "Edit Customer"
        TextView title = (TextView) findViewById(R.id.aecTitle);
        title.setText(getString(R.string.titleEditCustomer));

        Customer customer;
        try {
            // get UUID from intent and read customer info from randomUUID
            String uuidString = getIntent().getExtras().getString(Customer.EXTRA_UUID);
            customer = Customer.readFromDB(uuidString, dbHelper.getReadableDatabase());
        } catch (NullPointerException e){
            finish();
            return;
        }

        // fill in customer data
        // have id for customer id but will be hidden
        ((EditText) findViewById(R.id.uuidValue)).setText(customer.getUUID().toString());
        ((EditText) findViewById(R.id.aecFirstName)).setText(customer.getFirstName());
        ((EditText) findViewById(R.id.aecLastName)).setText(customer.getLastName());
        ((EditText) findViewById(R.id.aecAddress)).setText(customer.getBillingAddress());
        ((EditText) findViewById(R.id.aecEmail)).setText(customer.getEmailAddress());

        goldDiscountPercent = customer.getGoldDiscountPercent();
        availableCredit = customer.getAvailableCredit();
        availableCreditEarnDate = customer.getAvailableCreditEarnDate();

        // save button logic - save customer data
        final Button saveButton = (Button) findViewById(R.id.button_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Customer customer = new Customer(
                        UUID.fromString(((EditText) findViewById(R.id.uuidValue)).getText().toString()), // uuid
                        ((EditText) findViewById(R.id.aecFirstName)).getText().toString(), // first name
                        ((EditText) findViewById(R.id.aecLastName)).getText().toString(), // last name
                        ((EditText) findViewById(R.id.aecAddress)).getText().toString(), // billingAddress
                        ((EditText) findViewById(R.id.aecEmail)).getText().toString(), // emailAddress
                        goldDiscountPercent, // goldDiscountPercent
                        availableCredit, // availableCredit
                        availableCreditEarnDate // availableCreditEarnDate
                );

                EditText firstName = (EditText) findViewById(R.id.aecFirstName);
                EditText lastName = (EditText) findViewById(R.id.aecLastName);
                EditText email = (EditText) findViewById(R.id.aecEmail);


                if( firstName.getText().toString().trim().equals("") ) {
                    firstName.setError("Invalid First Name!");
                }else if( lastName.getText().toString().trim().equals("") ) {
                    lastName.setError("Invalid Last Name!");
                }else if( !isEmailValid(email.getText().toString())) {
                    email.setError("Invalid Email Address!");
                }else {
                    int result = CartManager.editCustomer(customer, getApplicationContext());
                    Intent resultIntent = new Intent();
                    setResult(result, resultIntent);
                    finish();
                }


            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelper.close();
    }


    public boolean isEmailValid(String email){
        if (email.isEmpty())
            return false;

        String regExpn = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }
}
