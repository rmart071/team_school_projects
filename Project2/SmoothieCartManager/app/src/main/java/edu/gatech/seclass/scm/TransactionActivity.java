package edu.gatech.seclass.scm;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import edu.gatech.seclass.scm.models.Customer;
import edu.gatech.seclass.services.EmailService;

public class TransactionActivity extends AppCompatActivity implements TextWatcher{

    private SmoothieCartDbHelper dbHelper = SmoothieCartDbHelper.getInstance(this);
    private UUID currentUUID = null;
    private long currentDate = 0;
    public Customer customer;
    double amount, rewardAvail, rewardUsed, discount, netSale;
    SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newsale);

        ((TextView) findViewById(R.id.nsGrossSale)).addTextChangedListener(this);

        try {
            // get UUID from intent and read customer info from randomUUID
            String uuidString = getIntent().getExtras().getString(Customer.EXTRA_UUID);
            customer = Customer.readFromDB(uuidString, dbHelper.getReadableDatabase());
        } catch (NullPointerException e){
            finish();
            return;
        }
        ((TextView) findViewById(R.id.nsCNQR)).setText(customer.getFirstName() + " "+customer.getLastName());
        currentUUID = customer.getUUID();
        currentDate = customer.getAvailableCreditEarnDate();

        //remove credit if it has been more than a year
        if(!isValid(customer.getAvailableCreditEarnDate()) && customer.getAvailableCredit() > 0){
            Customer customerUpdate = new Customer(
                    currentUUID, // uuid
                    customer.getFirstName(), // first name
                    customer.getLastName(), // last name
                    customer.getBillingAddress(), // billingAddress
                    customer.getEmailAddress(), // emailAddress
                    customer.getGoldDiscountPercent(), // goldDiscountPercent
                    0, // availableCredit
                    customer.getAvailableCreditEarnDate() // availableCreditEarnDate
            );
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            customerUpdate.updateDB(db);
            //read database again with updated changes
            try {
                // get UUID from intent and read customer info from randomUUID
                String uuidString = getIntent().getExtras().getString(Customer.EXTRA_UUID);
                customer = Customer.readFromDB(uuidString, dbHelper.getReadableDatabase());
            } catch (NullPointerException e){
                finish();
                return;
            }
        }

    }


    public void callPaymentProcess(View view){
        final Date dt = new Date();

        //handles the functionality of the process button being clicked
        //could implement a confirmation box
        // get credit card  information from selected customer

        new AlertDialog.Builder(this)
                .setTitle("Smoothie Cart Manager")
                .setMessage("Are you sure you want to process payment?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        long result = CartManager.processPurchases(customer, amount, rewardUsed, discount*100, netSale, getApplicationContext());
                        if (result > 0) {
                            updateCustomer(currentUUID, dt);
                        } else {
                            Toast.makeText(TransactionActivity.this, "Payment Error!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("No", null).show();

    }


    public void enterAmount(View view){
        /* first get the amount from the view
            then call the rewards amount and check if customer has gold status
            finally show final sale and ask to proceed
         */

        // get the amount from ui
        EditText amountTxt = (EditText) findViewById(R.id.nsGrossSale);

        // update values
        try {
            amount = Double.parseDouble(amountTxt.getText().toString());
        } catch (NumberFormatException e) {
            amount = 0;
        }
        rewardAvail = customer.getAvailableCredit();
        discount = customer.getGoldDiscountPercent();
        // (net - apply gold discount before reward discount)
        netSale = roundCent(calcRewardAmount(calcGoldDiscountAmount(amount)));

        // Update UI
        ((TextView) findViewById(R.id.rewardsText)).setText(moneyFormat(rewardUsed));
        ((TextView) findViewById(R.id.discountText)).setText(percentFormat(discount));
        ((TextView) findViewById(R.id.netSaleText)).setText(moneyFormat(netSale));

        // enable process
        ((Button) findViewById(R.id.processButton)).setEnabled(true);

    }

    private double roundCent(double amount) {
        return new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
    }

    private String moneyFormat(double amount) {
        return NumberFormat.getCurrencyInstance().format(amount);
    }

    private String percentFormat(double percent) {
        return NumberFormat.getPercentInstance().format(percent);
    }

    /**
     * Checks customer's rewards if any and applies to total amount. Saves amount used.
     * @param amount Total before applying rewards
     * @return Total after applying rewards
     */
    public double calcRewardAmount(double amount){

        // get the rewards for the user
        double reward = customer.getAvailableCredit();
        //handles the case when user's rewards are greater than current amount
        if(reward > amount) {
            rewardUsed = amount;
            return 0.0;
        } else {
            rewardUsed = reward;
            return (amount - reward);
        }
    }

    /**
     * Applies customer's gold status discount
     * @param amount Total before gold discount applied
     * @return Total after gold discount applied
     */
    public double calcGoldDiscountAmount(double amount){
        double credit = customer.getGoldDiscountPercent();
        return amount - (amount * credit);
    }


    /**
     * Updates customer's information if credit is attained
     * @param customerUUID Customer's QR Code ID
     * @param date Date of update due the tranaction
     */
    public void updateCustomer(UUID customerUUID, Date date){

        double finalReward = rewardAvail - rewardUsed;
        if(netSale >= 50) {
            finalReward = 5;
            date = new Date();
        }

        Customer customerUpdate = new Customer(
                customerUUID, // uuid
                customer.getFirstName(), // first name
                customer.getLastName(), // last name
                customer.getBillingAddress(), // billingAddress
                customer.getEmailAddress(), // emailAddress
                customer.getGoldDiscountPercent(), // goldDiscountPercent
                finalReward, // availableCredit
                date.getTime() // availableCreditEarnDate
        );
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if ( customerUpdate.updateGoldStatus(db) ) {
            sendGoldEmail(customerUpdate);
        }
        int result = (customerUpdate.updateDB(db))? Activity.RESULT_OK : Activity.RESULT_CANCELED;
        if(result == Activity.RESULT_OK) {
            Intent resultIntent = new Intent(TransactionActivity.this, MainActivity.class);
            setResult(result, resultIntent);
            finish();
        }else
            Toast.makeText(this, "Transaction failed!", Toast.LENGTH_SHORT).show();
    }

    private boolean sendGoldEmail(Customer customerUpdate) {
        boolean sent = false;
        int tries = 3;
        while( !sent && --tries > 0 ) {
            sent = EmailService.sendMail(customerUpdate.getEmailAddress(),
                    getString(R.string.goldEmailSubject),
                    getString(R.string.goldEmailBody));
        }
        return sent;
    }

    //checks if date of credit is within 1 year
    public boolean isValid(long timestamp) {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 12);
        java.util.Date expirationDate = cal.getTime();
        int diffInDays = (int)( (expirationDate.getTime() - timestamp) / (1000 * 60 * 60 * 24) );
        diffInDays = diffInDays-1;
        return diffInDays <= 365;
    }


    public void cancelTransaction(View view){
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        enterAmount(null);
    }

    @Override
    public void afterTextChanged(Editable s) {}
}
