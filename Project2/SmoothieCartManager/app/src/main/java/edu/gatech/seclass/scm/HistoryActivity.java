package edu.gatech.seclass.scm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.UUID;

import edu.gatech.seclass.scm.models.Customer;
import edu.gatech.seclass.scm.models.Transaction;


public class HistoryActivity extends AppCompatActivity {
    private SmoothieCartDbHelper dbHelper = SmoothieCartDbHelper.getInstance(this);
    private Customer customer;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showhistory);

        try {
            // get UUID from intent and read customer info from randomUUID
            String uuidString = getIntent().getExtras().getString(Customer.EXTRA_UUID);
            customer
                    = Customer.readFromDB(uuidString, dbHelper.getReadableDatabase());
        } catch (NullPointerException e){
            finish();
            return;
        }

        ((TextView) findViewById(R.id.shCNQR)).setText(customer.getFirstName() + " "+customer.getLastName());

        ArrayList<Transaction> transactionsArray = new ArrayList<Transaction>();
        //call the list customer transactions method from the cart manager
        transactionsArray = CartManager.listCustomerTransactions(customer, getApplicationContext());
        ListView lv = (ListView) findViewById(R.id.shSaleDetail);

        lv.setAdapter(new ArrayAdapter<Transaction>(this,
                android.R.layout.activity_list_item, android.R.id.text1, transactionsArray));

    }


    public void doneHistory(View view){
        finish();
    }
}
