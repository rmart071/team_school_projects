package edu.gatech.seclass.scm;

import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.UUID;

import edu.gatech.seclass.scm.models.Customer;
import edu.gatech.seclass.services.QRCodeService;

public class MainActivity extends AppCompatActivity {

    public static final int ADD_CUSTOMER_REQUEST = 1;
    public static final int EDIT_CUSTOMER_REQUEST = 2;
    public static final int PROCESS_PAYMENT = 3;

    private SmoothieCartDbHelper dbHelper = SmoothieCartDbHelper.getInstance(this);
    private UUID currentUUID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        updateUI();

    }

    private void updateUI() {

        // show info in text fields
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        try {
            Customer c = Customer.readFromDB(currentUUID.toString(), db);
            ((EditText) findViewById(R.id.cmCNQR)).setText(c.getFirstName() + " " + c.getLastName());
        } catch (NullPointerException e) {
            ((EditText) findViewById(R.id.cmCNQR)).setText("");
        } catch (CursorIndexOutOfBoundsException e) {
            // UUID read, but not in DB
            String err = ("No such customer! UUID: " + currentUUID.toString());
            Toast.makeText(this, err, Toast.LENGTH_LONG).show();
            currentUUID = null;
            ((EditText) findViewById(R.id.cmCNQR)).setText("");
        }

        // enable/disable buttons
        boolean haveUUID = (currentUUID != null);
        ((Button) findViewById(R.id.button_editcustomer)).setEnabled(haveUUID);
        ((Button) findViewById(R.id.button_newsale)).setEnabled(haveUUID);
        ((Button) findViewById(R.id.button_showhistory)).setEnabled(haveUUID);
    }

    public void launchAddCustomer(View view) {
        Intent addCustomerView = new Intent(this, AddCustomer.class);
        startActivityForResult(addCustomerView, ADD_CUSTOMER_REQUEST);
    }

    public void launchEditCustomer(View view) {
        Intent editCustomerView = new Intent(this, EditCustomer.class);
        String uuidString = currentUUID.toString();
        editCustomerView.putExtra(Customer.EXTRA_UUID, uuidString);
        startActivityForResult(editCustomerView, EDIT_CUSTOMER_REQUEST);
    }


    public void launchTransaction(View view) {
        Intent transactionView = new Intent(MainActivity.this, TransactionActivity.class);
        String uuidString = currentUUID.toString();
        transactionView.putExtra(Customer.EXTRA_UUID, uuidString);
        startActivityForResult(transactionView, PROCESS_PAYMENT);
    }

    public void launchShowHistory(View view) {
        Intent historyView = new Intent(MainActivity.this, HistoryActivity.class);
        String uuidString = currentUUID.toString();
        historyView.putExtra(Customer.EXTRA_UUID, uuidString);
        startActivity(historyView);
    }


    public void scanQR(View view) {
        String raw = QRCodeService.scanQRCode();
        if ( raw.compareTo("ERR") != 0 && raw.length() == 32 ) {

            long msb = Long.parseLong(raw.substring(0, 8), 16) << 32 |
                    Long.parseLong(raw.substring(8,16), 16);
            long lsb = Long.parseLong(raw.substring(16, 24), 16) << 32 |
                    Long.parseLong(raw.substring(24,32), 16);

            currentUUID = new UUID(msb, lsb);
        } else {
            String err = ("Error reading QR code; please try again.");
            Toast.makeText(this, err, Toast.LENGTH_LONG).show();
        }
        updateUI();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ADD_CUSTOMER_REQUEST:
                if (resultCode == RESULT_OK) {
                    String uuidString = data.getExtras().getString(Customer.EXTRA_UUID);
                    currentUUID = UUID.fromString(uuidString);
                    Toast.makeText(this, "Customer added to System!", Toast.LENGTH_SHORT).show();
                }
                break;
            case EDIT_CUSTOMER_REQUEST:
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "Update to Customer Successful!", Toast.LENGTH_SHORT).show();
                }
                break;
            case PROCESS_PAYMENT:
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "Payment successful!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        updateUI();
    }
}
