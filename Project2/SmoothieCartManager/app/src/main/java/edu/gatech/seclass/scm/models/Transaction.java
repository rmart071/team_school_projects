package edu.gatech.seclass.scm.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

//import edu.gatech.seclass.services.PaymentService;

/**
 *
 */
public class Transaction {
    private long transactionID;
    private UUID customerID;
    private long transactionDate;
    private double grossSaleAmount;
    private double rewardDiscountAmount;
    private double goldDiscountAmount;
    private double netSaleAmount;

    private String fName;
    private String lName;
    private String ccNum;
    private String securityCode;

    public double getNetSaleAmount() {
        return netSaleAmount;
    }

    public long getTransactionID() {
        return transactionID;
    }

    public UUID getCustomerID() {
        return customerID;
    }

    public long getTransactionDate() {
        return transactionDate;
    }

    public double getGrossSaleAmount() {
        return grossSaleAmount;
    }

    public double getRewardDiscountAmount() {
        return rewardDiscountAmount;
    }

    public double getGoldDiscountAmount() {
        return goldDiscountAmount;
    }

    public Transaction(
            long transactionID,
            UUID customerID,
            long transactionDate,
            double grossSaleAmount,
            double rewardDiscountAmount,
            double goldDiscountAmount,
            double netSaleAmount
    ) {
        this.transactionID = transactionID;
        this.customerID = customerID;
        this.transactionDate = transactionDate;
        this.grossSaleAmount = grossSaleAmount;
        this.rewardDiscountAmount = rewardDiscountAmount;
        this.goldDiscountAmount = goldDiscountAmount;
        this.netSaleAmount = netSaleAmount;
    }

    public long writeToDB(SQLiteDatabase db) {

        // create row from customer data
        ContentValues values = new ContentValues();
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_CUSTOMER_UUID, this.customerID.toString());
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_DATE, this.transactionDate);
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_GROSS_SALE_AMOUNT, this.grossSaleAmount );
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_REWARD_DISCOUNT_AMOUNT, this.rewardDiscountAmount );
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_GOLD_DISCOUNT_AMOUNT, this.goldDiscountAmount );
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_NET_SALE_AMOUNT, this.netSaleAmount );

        // insert customer
        this.transactionID = db.insert(SmoothieCartContract.TransactionEntry.TABLE_NAME, null, values);
        return this.transactionID;
    }

    public static ArrayList readTransactions(String uuidString, SQLiteDatabase db) {

        // construct, execute query
        String table = SmoothieCartContract.TransactionEntry.TABLE_NAME;
        String where = SmoothieCartContract.TransactionEntry.COLUMN_NAME_CUSTOMER_UUID + " = ? ";
        String[] whereArgs = new String[]{ uuidString };
        Cursor cursor = db.query(table, null, where, whereArgs, null, null, null);

        ArrayList<Transaction> transactions2 = new ArrayList<>();
        LinkedList<Transaction> transactions = new LinkedList<>();

        // access 1st (and should be only!) row
        cursor.moveToFirst();

        while ( ! cursor.isAfterLast() ) {

            // pull data
            long transactionID = cursor.getLong(cursor.getColumnIndexOrThrow(
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_ID));
            UUID customerID = UUID.fromString(cursor.getString(cursor.getColumnIndexOrThrow(
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_CUSTOMER_UUID)));
            long transactionDateLong = cursor.getLong(cursor.getColumnIndexOrThrow(
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_DATE) );
            double grossSaleAmount = cursor.getDouble(cursor.getColumnIndexOrThrow(
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_GROSS_SALE_AMOUNT) );
            double rewardDiscountAmount = cursor.getDouble( cursor.getColumnIndexOrThrow(
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_REWARD_DISCOUNT_AMOUNT ) );
            double goldDiscountAmount = cursor.getDouble( cursor.getColumnIndexOrThrow(
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_GOLD_DISCOUNT_AMOUNT ) );
            double netSaleAmount = cursor.getDouble( cursor.getColumnIndexOrThrow(
                    SmoothieCartContract.TransactionEntry.COLUMN_NAME_NET_SALE_AMOUNT ) );

            // append new transaction
            transactions2.add(new Transaction(
                    transactionID,
                    customerID,
                    transactionDateLong,
                    grossSaleAmount,
                    rewardDiscountAmount,
                    goldDiscountAmount,
                    netSaleAmount));
            // increment cursor
            cursor.moveToNext();
        }

        // return as array
        return transactions2;
    }

    public static Transaction readTransaction(long transactionID, SQLiteDatabase db) {

        // construct, execute query
        String table = SmoothieCartContract.TransactionEntry.TABLE_NAME;
        String where = SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_ID + " = ? ";
        String[] whereArgs = new String[]{ Long.toString(transactionID) };
        Cursor cursor = db.query(table, null, where, whereArgs, null, null, null, "1");

        // pull data
        UUID customerID = UUID.fromString(cursor.getString(cursor.getColumnIndexOrThrow(
                SmoothieCartContract.TransactionEntry.COLUMN_NAME_CUSTOMER_UUID)));
        long transactionDateLong = cursor.getLong(cursor.getColumnIndexOrThrow(
                SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_DATE) );
        double grossSaleAmount = cursor.getDouble(cursor.getColumnIndexOrThrow(
                SmoothieCartContract.TransactionEntry.COLUMN_NAME_GROSS_SALE_AMOUNT) );
        double rewardDiscountAmount = cursor.getDouble( cursor.getColumnIndexOrThrow(
                SmoothieCartContract.TransactionEntry.COLUMN_NAME_REWARD_DISCOUNT_AMOUNT ) );
        double goldDiscountAmount = cursor.getDouble( cursor.getColumnIndexOrThrow(
                SmoothieCartContract.TransactionEntry.COLUMN_NAME_GOLD_DISCOUNT_AMOUNT ) );
        double netSaleAmount = cursor.getDouble(cursor.getColumnIndexOrThrow(
                SmoothieCartContract.TransactionEntry.COLUMN_NAME_NET_SALE_AMOUNT));

        // return new transaction
        return new Transaction(
                transactionID,
                customerID,
                transactionDateLong,
                grossSaleAmount,
                rewardDiscountAmount,
                goldDiscountAmount,
                netSaleAmount );
    }

    public void callPaymentProcess() {

        // TODO: finish this is just a stub!

//        fName = "test";
//        lName = "test";
//        ccNum = "4242424242424242";
//
//        Double amount = 100.252;
//        Date exDate = new Date();
//
//        boolean paymentStatus = PaymentService.processTransaction(fName, lName, ccNum, exDate, securityCode, amount);

    }

    public void applyAvailableReward() {
        // TODO
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void saveTransaction(SQLiteDatabase db) {

        // create row from transaction data
        ContentValues values = new ContentValues();
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_CUSTOMER_UUID, customerID.toString() );
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_TRANSACTION_DATE, transactionDate);
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_GROSS_SALE_AMOUNT, grossSaleAmount);
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_REWARD_DISCOUNT_AMOUNT, rewardDiscountAmount);
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_GOLD_DISCOUNT_AMOUNT, goldDiscountAmount);
        values.put( SmoothieCartContract.TransactionEntry.COLUMN_NAME_NET_SALE_AMOUNT, netSaleAmount);

        // insert transaction
        transactionID = db.insert(SmoothieCartContract.TransactionEntry.TABLE_NAME, null, values);
    }

    @Override
    public String toString(){
        DecimalFormat df = new DecimalFormat("#.00");
        Date date = new Date(getTransactionDate()); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy"); // the format of your date
        String formattedDate = sdf.format(date);
        System.out.println(formattedDate);
        return "Transaction Date: "+formattedDate+
                "\nGross Amount: $"+df.format(getGrossSaleAmount())+
                "\nRewards Applied: $"+df.format(getRewardDiscountAmount())+
                "\nDiscount Applied: "+getGoldDiscountAmount()+"%"+
                "\nNet Sale: $"+df.format(getNetSaleAmount());
    }
}
