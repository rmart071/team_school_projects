package edu.gatech.seclass.scm;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import edu.gatech.seclass.scm.models.Customer;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import edu.gatech.seclass.scm.models.SmoothieCartContract;
import edu.gatech.seclass.scm.models.Transaction;
import edu.gatech.seclass.services.CreditCardService;
import edu.gatech.seclass.services.PaymentService;

/**
 *
 */
public class CartManager {

    public static void addCustomer(Customer customer, Context context) {
        SQLiteDatabase db = SmoothieCartDbHelper.getInstance(context).getWritableDatabase();
        customer.writeToDB(db);
    }

    public static int editCustomer(Customer customer, Context context){
        SQLiteDatabase db = SmoothieCartDbHelper.getInstance(context).getReadableDatabase();
        return (customer.updateDB(db))? Activity.RESULT_OK : Activity.RESULT_CANCELED;
    }

    public static long processPurchases(Customer customer, double gross, double reward,
                                 double discount, double netSale, Context context){
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");
            String cc = CreditCardService.readCard();
            //let get variables needed for payment service
            String[] info = cc.split("#");

            Date date = formatter.parse(info[3]);
            Date dt = new Date();
            // if payment is successful update database

            if (PaymentService.processTransaction(info[0], info[1],
                    info[2], date, info[4], netSale)) {

                Transaction transaction = new Transaction(0,
                        customer.getUUID(), dt.getTime(), gross, reward, discount, netSale);
                SQLiteDatabase db = SmoothieCartDbHelper.getInstance(context).getWritableDatabase();
                long result = transaction.writeToDB(db);

                //updateCustomer(currentUUID, date);
                return result;

            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            // happens if CreditCardService.readCard() returns "ERR" - info[] will be len 1
        }
        return 0;

    }

    public static Customer accessCustomer(String uuidString, Context context) {
        SQLiteDatabase db = SmoothieCartDbHelper.getInstance(context).getReadableDatabase();
        return Customer.readFromDB(uuidString, db);
    }

    public static ArrayList<Transaction> listCustomerTransactions(Customer customer, Context context) {
        ArrayList<Transaction> transactionsArray = new ArrayList<Transaction>();
        SQLiteDatabase db = SmoothieCartDbHelper.getInstance(context).getWritableDatabase();

        transactionsArray = Transaction.readTransactions(customer.getUUID().toString(),db);
        return transactionsArray;
    }
}
