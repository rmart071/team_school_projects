package edu.gatech.seclass.scm;

import android.content.Intent;
import android.widget.Button;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class AddCustomerTest {
    AddCustomer customer = new AddCustomer();
    @Test
    public void isValidTest1() throws Exception {

        String email_test = "";
        assertFalse(customer.isEmailValid(email_test));
    }

    @Test
    public void isValidTest2() throws Exception {
        String email_test = "John Smith";
        assertFalse(customer.isEmailValid(email_test));
    }


    @Test
    public void isValidTest3() throws Exception {
        String email_test = "jsmith@gatech";
        assertFalse(customer.isEmailValid(email_test));
    }

    @Test
    public void isValidTest4() throws Exception {
        String email_test = "jsmith@gatech.edu";
        assertTrue(customer.isEmailValid(email_test));
    }

}