package edu.gatech.seclass.scm;


import org.junit.Test;

import java.util.Date;
import java.util.UUID;

import edu.gatech.seclass.scm.models.Customer;
import edu.gatech.seclass.scm.models.Transaction;

import static org.junit.Assert.*;
public class TransactionActivityTest {

    TransactionActivity transaction = new TransactionActivity();


    @Test
    public void calcRewardAmountTest1() throws Exception {
        Customer customer = new Customer(
                UUID.randomUUID(),
                "John", // first name
                "Smith", // last name
                "", // billingAddress
                "", // emailAddress
                0, // goldDiscountPercent
                5, // availableCredit
                0 // availableCreditEarnDate
        );

        double amount = 57;
        transaction.customer = customer;

        assertEquals(52.0,transaction.calcRewardAmount(amount),5);
    }

    @Test
    public void calcRewardAmountTest2() throws Exception {
        Customer customer = new Customer(
                UUID.randomUUID(),
                "John", // first name
                "Smith", // last name
                "", // billingAddress
                "", // emailAddress
                0, // goldDiscountPercent
                0, // availableCredit
                0 // availableCreditEarnDate
        );

        double amount = 43;
        transaction.customer = customer;

        assertEquals(43,transaction.calcRewardAmount(amount),5);
    }

    @Test
    public void calcRewardAmountTest3() throws Exception {
        Customer customer = new Customer(
                UUID.randomUUID(),
                "John", // first name
                "Smith", // last name
                "", // billingAddress
                "", // emailAddress
                0, // goldDiscountPercent
                5, // availableCredit
                0 // availableCreditEarnDate
        );

        double amount = 3.50;
        transaction.customer = customer;

        assertEquals(0,transaction.calcRewardAmount(amount),5);
    }

    @Test
    public void calcGoldDiscountAmountTest1() throws Exception {
        Customer customer = new Customer(
                UUID.randomUUID(),
                "John", // first name
                "Smith", // last name
                "", // billingAddress
                "", // emailAddress
                0.05, // goldDiscountPercent
                0, // availableCredit
                0 // availableCreditEarnDate
        );

        double amount = 50;
        transaction.customer = customer;

        assertEquals(47.50,transaction.calcGoldDiscountAmount(amount),5);
    }

    @Test
    public void isValidTest1() throws Exception {
        Customer customer = new Customer(
                UUID.randomUUID(),
                "John", // first name
                "Smith", // last name
                "", // billingAddress
                "", // emailAddress
                0.05, // goldDiscountPercent
                0, // availableCredit
                1447471899000L // availableCreditEarnDate
        );


        double amount = 50;
        transaction.customer = customer;
        Date dt = new Date();

        assertTrue(transaction.isValid(customer.getAvailableCreditEarnDate()));
    }

    @Test
    public void isValidTest2() throws Exception {
        Customer customer = new Customer(
                UUID.randomUUID(),
                "John", // first name
                "Smith", // last name
                "", // billingAddress
                "", // emailAddress
                0.05, // goldDiscountPercent
                0, // availableCredit
                1384399899000L // availableCreditEarnDate
        );


        double amount = 50;
        transaction.customer = customer;
        Date dt = new Date();

        assertFalse(transaction.isValid(customer.getAvailableCreditEarnDate()));
    }
}
