
Design Discussion - Team 36
=================

Individual Designs:
-------------------

####Keason Design:

![Keason Design](ksanvordenker3-design.png)

Pros:
 - Clean, easy to understand
 - Gold system calculation
 - Simple transaction, no itemization

Cons: 
 - One-to-many transaction/customer
 - QR code class

####Austin design:

![Austin Design](aclick7-design.png)

Pros: 
 - QR scanner
 - Discount inheritance

Cons: 
 - Gold status system
 - Too many classes
 - PaymentProcessor CreditCard, CardReader relationship - too many classes/too complicated for this task

####Ryan Design:

![Ryan Design](rfitzgerald9-design.png)

Pros: 
 - Good address scheme
 - Credit card scanner encapsulates payment processing service provider

Cons: 
 - `editCustomerInformation()` unnecessary - leave edits in Customer
 - Need to store reward status class
 - Orders don't need multiple items

####Ricardo Design:

![Ricardo Design](rmartinez6-design.png)

Pros:
 - Email service abstracted
 - rewards broken out to gold status/credit

Cons:
 - Too many unclear relationships
 - Unclear why reward classes call email interface directly

Team Design:
------------

![Team 36 Design](team36-design.png) 

###Commonalities and Differences

####Commonalities
We took the best features of each design and simplified them. We were able to reduce the number of classes from each other individual designs, eventually settling on a Cart Manager, Customer, and Transaction classes which were common to all of the designs. The requirements for QR reader and how it was to be handled , the details of the payment processor and email were not known so we settled on a simple design with only the functions necessary (no error handling). For the credit card processor utility class, we discussed how to handle errors such as no-transmit, not-read, and approval/ rejections codes. 

####Differences
As part of the simplification process, we eliminated the classes not related to the specific requirements, such as the item and OrderLine class in Ryan's design. We also eliminiated the credit card class in Ricardo's and Austin's design, and the verification and getDate utility classes in several of the designs. We were not sure if the date would be manually entered, and will add the Operating system utility class back depending if requirements change.

All of the individual designs varied on the handling of the gold status and calculating rewards from the transactions. Several of the designs used a boolean field for gold status, and others used a numeric value for the discount. Since the difference is likely just coding, we settled on using a numeric discount value. The same applied for transactions and reward credit. We varied on handling the expiration of the credit, with one design linking the reward amount and the expiration date. The problem occurred on how to handle carry over credits. We decided to table the discussion until we have further clarification. 

We decided to split the billing address customer attribute into several fields to simplify the generation of the body text (e.g. listing the address line by line) and in case there were duplicate names, it would be easier to search if the QR code is not available.





Summary:
--------

It was interesting to have the opportunity to look at designs from different perspectives, to pick up each designs strengths, and discuss them. We learned that we need to come up with the distribution of work early on. We thought it was a little unrealistic to come up with a design in isolation, and then come together once the designs are complete. We learned that classes which are important to one design may not even appear in another. Further, classes may be rendered completely unnecessary based on how they are used. For example, half the designs had `Discount` classes, which were unnecessary in our team's final design.

