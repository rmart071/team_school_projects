Assignment 5
============

*Austin Click | GTID# 902168286*


Customer, Transaction, Discount
-------------------------------

Customers, Transactions and Discounts form the backbone of the system. The cart system is able to have 0..* Customers, each with a log of 1..* Transactions, which may each have 0..* Discounts applied. Customers are added on their first transaction, so I made that relationship a 1..* instead of 0..*. 

There are two types of discounts described in the requirements - a credit given for a large enough purchase (CreditDiscount), and a percentage discount for having gold status (GoldStatusDiscount). I added a getPriority() method, which are implemented by these subclasses to indicate to the logic processing the Transaction which discounts to apply first. I also contained the logic of how to compute the discount in the discount classes themselves, so that new discounts can be added to the system trivially. 

The transaction would be given a set of discounts and orderPrice (the price of the transaction pre-discount), and would contain the logic to calculate the total (getTotal) based on that amount and applicable discounts. 

I assume Customer has getters and setters, so that the customer information can be edited after being accessed using the accessCustomer(customerID) method of the CartSystem. 


EmailSystem
-----------

I decided to make this a distinct interface class, since in the real world, this is probably how it would work. Many email systems could be supported, but have the interface abstracted to just what the cart system would care about - given a customer (containing email address) send emails regarding gold status/discounts. 


PaymentProcessor
----------------

Similar to EmailSystem, I wanted to abstract this to an interface, just exposing a process() method to the rest of the system. The system would generate the Transaction and the read the CreditCard, then pass this to the interface. The interface would use getTotal() and the (immutable member) credit card information to process the purchase. It would return status provided by the credit card processor (e.g. accepted or rejected transaction). 


QRScanner, CardReader
---------------------

These are more interface classes, allowing the system to only be exposed the scanCustomerCard()/scanCreditCard() methods. The methods would return the customer ID as scanned from the QR code on the customer card, or the credit card information contained on the customers credit card, or some error status indicating the problem. Again, these interfaces allow for complex implementation details of these systems to remain logically separated and abstracted away from the system. 


QR Code, Customer Card
----------------------

At first, I included these in the class diagram, but upon further reflection, I decided to take them off. My rationale is that, as I read the document, the cards are premade with serial id numbers, and the system only cares about the unique ID that is printed on the card. The system does not generate a code, and the card contains no other information. Therefore, I thought it was best to leave it as single attribute on the customer (ID number)


