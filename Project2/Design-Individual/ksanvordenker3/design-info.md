#Additional Notes and Assumptions on UML Class Diagram (ksanvordenker3)

##1 Assumptions

	1.1 Utility class exists and provides current date to populate transaction class and provide basis for calculating reward amount
		
	1.2 Payment Processor in cloud provides binary approval upon submission of CC information
	
	1.3 CC Reader, QR reader and QR sticker printer for customer cards are provided. Assumption is that QR codes are not already
		in existence and will not need to be matched with customer number
		
	1.4 Rewards expire after one year. Available rewards will be calculated based on rewards earned in past
	 year-rewards used in past year
	 
	1.5 Not necessary to store any further credit card information, payment approval number etc....
	 
##2 Class Assumptions

	2.1 Customer class - billing address split due to potential unstated requirement to use zip code validation
	 in CC processing, plus possible need to create a unique index for lookup of customers who may have similar names
	
	2.2 Transaction class - Transaction not created if payment not approved. No information for failed transactions is retained.	
		
	
	