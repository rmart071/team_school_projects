# Use Case Model


**Author: Team 36**

## 1 Use Case Diagram

![Team 36 Use Case Diagram](Use_Case_Diagram.png)

## 2 Use Case Descriptions


- **Name**: Add Customer
- **Requirements**: Allow the manager to add customers to the system.
- **Pre-conditions**: Customer is not in the system
- **Post-conditions**: The customer is now in the system and can make purchases.
- **Scenarios**:
	1. Manager selects on Add Customer button
	2. Manager enters the customer's information
		- *Address*
		- *City*
		- *State*
		- *Email Address*
	3. Manager submits information
	4. System saves information
	6. System produces customer card

======

- **Name**: Assign QR code Customer
- **Requirements**: Provide the customer with a QR code.
- **Pre-conditions**: Customer is not in the system
- **Post-conditions**: The customer is now in the system and can be identified using QR code.
- **Scenarios**:
	1. Manager clicks on Add Customer button
	2. Manager enters the customer's information
		- *Address*
		- *City*
		- *State*
		- *Email Address*
	3. Manager submits information
	4. Manager selects Print QR button
	6. System produces customer card

======

- **Name**: Scan QR code
- **Requirements**: Allow the manager to scan the user's QR code.
- **Pre-conditions**: Customer already exists in the system and has QR code.
- **Post-conditions**: The system now displays the customer's information.
- **Scenarios**:
	1. Manager selects Scan QR button
	2. System redirects to scan screen
	3. Manager scan's users QR code
	4. System redirects back to home screen with customer name and QR code
	

======


- **Name**: Look up Customer
- **Requirements**: Allow the manager to look up customer information without QR code.
- **Pre-conditions**: Customer already exists in the system.
- **Post-conditions**: The system now displays the customer's information.
- **Scenarios**:
	1. Manager selects Lookup button
	2. System redirects Lookup screen
	3. Manager enters customer's email address
	4. System returns list of customer with matching information
	5. Manager selects correct customer
	6. System redirects back to screen with customer name
	

======

- **Name**: Edit Customer
- **Requirements**: Allow the manager to update customer information.
- **Pre-conditions**: Customer's information has been uploaded by Lookup or QR code.
- **Post-conditions**: The customer's updated information is now saved.
- **Scenarios**:
	1. Manager selects Edit Customer button
	2. System populates text boxes with customer information
	3. Manager updates customer's information
	4. Manager saves changes
	

======

- **Name**: Show History
- **Requirements**: Allow the manager to view all purchases and rewards made from the customer.
- **Pre-conditions**: Customer already exists in the system.
- **Post-conditions**: The manager can now see all of the customer's purchases and rewards.
- **Scenarios**:
	1. Manager selects customer name
	2. Manager selects View Purchases/Rewards button
	3. System returns all purchases made by customer
	4. System returns date of purchase, amount of purchase before discount, and how much was discount if applicable
	4. System presents information on view
		

======

- **Name**: Make Payment with scanner
- **Requirements**: Allow the manager to make transaction.
- **Pre-conditions**: The customer already exists in the system
- **Post-conditions**: Customer can now enjoy their purchases.
- **Scenarios**:
	1. Manager selects New Sale button
	2. Manager enters Gross Sale
	3. System checks if user has 'Gold' status
	4. System retrieves any additional rewards
	5. System applies rewards and displays Net amount
	6. Manager selects process
	7. System notifies manager to swipe card
	8. System accepts payment
	9. Payment is finalized
	10. System redirects back to home screen
	

======

- **Name**: Make Payment with payment-processing service provider
- **Requirements**: Allow the manager to make purchase.
- **Pre-conditions**: The customer already exists in the system
- **Post-conditions**: Customer can now enjoy their purchases.
- **Scenarios**:
	1. Manager selects item
	2. Manager finalizes order
	3. Manager shows amount for purchase to customer
	4. Manager selects provider to make payment
	5. Customer enters information to acknowledge provider
	6. System accepts payment
	7. System applies discount if applicable
	8. System checks if discount is earned
	9. Payment is finalized
	

	
