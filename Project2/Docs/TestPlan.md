# Test Plan

<!-- *This is the template for your test plan. The parts in italics are concise explanations of what should go in the corresponding sections and should not appear in the final document.* -->

**Author**: Team 36

## 1 Testing Strategy

### 1.1 Overall strategy

<!-- *This section should provide details about your unit-, integration-, system-, and regression-testing strategies. In particular, it should discuss which activities you will perform as part of your testing process, and who will perform such activities.* -->

We will be using a combined approach of several software testing strategies. All early testing will be white-box techniques.  We will start with unit testing, before and during development, in which the programmers of individual sections are responsible for creating Unit tests, and utilizing them to write testable code. To further test code after it has been completed, we will utilize code reviews, or inspections. To the extent that it is possible, a test suite which allows for regression testing will be implemented and maintained over the life of the project. 

Once the application has progressed to a usable state, black-box system testing will begin. Due to the nature of the project, acceptance testing and beta testing will not be possible. All testing will be performed by the team of developers, and our "acceptance testing" can be considered our deliverable. System tests will be based on our requirements document, and will verify high-level functionality of the system as a whole. As failures are found, they will be entered into and tracked by Github's integrated issue tracking system. 

### 1.2 Test Selection


We will select our test cases through systematic functional testing. We will use relevant inputs to create test case specifications, and then narrow these down to create a set of concrete test cases. For example, testing the reward credit system can be broken down to gold status, dollar amount of purchase, and amount of credit. This wide input domain can be sectioned into combinations of the cases in which: The amount is less than $50, or the amount is greater than $50; The credit amount is less than, or greater than the purchase amount; ;Gold status is true or false; etc. Cases that make no sense combined are eliminated, and the domain is covered efficiently. 

### 1.3 Adequacy Criterion


We will design a test suite from the requirement specifications, and analyze our coverage of the input domains (as described above, white-box) to ensure coverage during our initial unit test suite. As such, the suite will adapt as our code is implemented. We will be using a strategy of specification-based, black-box testing for system testing. In this way, we intend to create a broad coverage based on both our client's specifications, as well as our code for better, but still efficient, coverage.

### 1.4 Bug Tracking


We will create and use a shared google docs table to track bugs and enhancement requests. 

### 1.5 Technology

We will use JUnit for the creation of our software test suite, and Github's integrated issue tracking system to track bugs and failures discovered in testing. We also intend to use automated Espresso test cases that cover the cases listed below in this document where possible, and may use Barista to generate them.

## 2 Test Cases

### 2.1 System Testing Procedure

<!-- *This section should be the core of this document. You should provide a table of test cases, one per row. For each test case, the table should provide its purpose, the steps necessary to perform the test, the expected result, the actual result (to be filled later), pass/fail information (to be filled later), and any additional information you think is relevant.* -->
TC = Test Case


| TC ID | Purpose | Procedure | Expected Result | Actual Result | Pass/Fail |
|:-------|:------- |:--------- |:--------------- |:------------- |:---------:|
| 1.1 | Verify *Add Customer* functionality | Click on registration button and enter customer's information (see table 1). Submit information. | New user is shown on the application | System redirects to home page and shows name on top of screen | **PASS** |
| 1.2 | Verify *Add Customer* does not add existing customer | Click on registration button and enter customer's information (see table 1). Save button is pressed. | System checks database to see if user already exists and outputs message indicating so. No customer is added  | System allows duplicate entry with unique UUID | **PASS** |
| 1.3 | Verify *Add Customer* functionality | Click on registration button and press save button with no information filled. | System checks if input are empty and shows message indicating so. No customer is added  | System shows error in Fname field. User may correct or cancel | **PASS** |
| 3.1 | Verify *Scan QR* functionality | Select Scan QR button on home screen | System scans QR code and displays customer information. | Same|**PASS** |
| 5.1 | Verify *Edit Customer* functionality | Select customer name (from table 1) in customer list interface. Select update information button. Change customer information to match data in table 2. Save Changes. Verify the customer's updated information is present. | Customer's information is updated. |same |**PASS** |
| 5.2 | Verify *Edit Customer* does not accept changes with empty fields | Select customer name (from table 1) in customer list interface. Select update information button. Erase all data and select save. | System shows error in Fname text field | same|**PASS** |
| 5.3 | Verify *Edit Customer* does not accept changes if cancel is selected | Select customer name (from table 1) in customer list interface. Select update information button. Update client information to match table 2 but select cancel button. | System redirects back to main screen and does not update data |same |**PASS** |
| 6.1 | Verify ability to list customer purchases and rewards | Use customer 1 (see table 1). If customer has not been entered, create the customer entry (see Verify *Add Customer* functionality test procedure). Selects View Purchases/Rewards button. Verify the list is empty. Enter the purchases from table 3. Verify system returns date of purchase, amount of purchase before discount, and how much was discount if applicable. | System presents correct purchase, discount information | same| **PASS**|
| 6.2 | Verify ability to list customer purchases and rewards | Scan QR Code. Select Show History button. | System presents correct purchase, discount information | same|**PASS** |
| 6.3 | Verify ability to not show purchases or rewards if empty | Scan QR Code. Select Show History button. | System shows empty list indicating no purchases have been made |same |**PASS** |
| 7.1 | Verify ability to make payment with scanner | Select "New Sale" button. Enter gross sale (line 1 table 3). Select "Process" and swipe card. | System accepts payment, returns payment status message and redirects back to home screen. |same |**PASS** |
| 8.1 | Verify ability to make payment with payment-processing service provider | Enter order (1st line, table 3), and finalize order. Select provider to make payment, Click on process. Service provider accepts payment and returns notification to system |  System saves information. Repeat per table |same  |**PASS** |
| 8.2 | Verify ability to not allow transaction if not approved by service provider | Enter order (1st line, table 3), and finalize order. Select Process to make payment. Service provider does not accepts payment. Remains on New Sale Screen. Press Cancel button, then Show History to confirm payment was not processed | No transaction is posted on system if declined. | same| **PASS** |




### 2.2 Test Case Data

####**Table 1a**

| | |
|:----------------- |:----------------- |
| *First Name*      | Bud               |
| *Last Name*       | Peterson          |
| *Email Address*   | bpeterson@gt.com  |
| *Address Line*  | 225 North Avenue, Atlanta GA 30332  |


####**Table 1b**

| | |
|:----------------- |:----------------- |
| *First Name*      | Ray               |
| *Last Name*       | Watts             |
| *Email Address*   | rwatts@gt.com  	|
| *Address Line*  | 2nd Avenue South, Birmingham AL 35233  |

####**Table 2a - pre-programmed UUID**

| | |
|:----------------- |:----------------- |
| *First Name*      | Ralph              |
| *Last Name*       | Hapschatt           |
| *Email Address*   | rhapchatt@gt.com  	|
| *Address Line*  | 389  2nd Avenue South, Birmingham AL 35233  |

####**Table 2b - pre-programmed UUID**

| | |
|:----------------- |:----------------- |
| *First Name*      | Everett              |
| *Last Name*       | Scott           |
| *Email Address*   | escott@gt.com  	|
| *Address Line*  | 12882 Peachtree Circle, Atlanta GA 30330  |

####**Table 2c - pre-programmed UUID**

| | |
|:----------------- |:----------------- |
| *First Name*      | Betty           |
| *Last Name*       | Monroe           |
| *Email Address*   | bmonroe@gt.com  	|
| *Address Line*  | 14881 Memorial Parkway, Norcross GA 30221  |





####**Table 3**

| Purchase Amount | Existing Rewards Balance | Discount | Gold Status | Price After Discount | Credit Earned |
| ---------------:| ------------------------:| --------:|:-----------:| --------------------:| -------------:|
| $5.00           | $0.00                    | $0.00    | No          | $5.00                | $0.00         |
| $50.00          | $0.00                    | $0.00    | No          | $50.00               | $5.00         |
| $55.00          | $5.00                    | $5.00    | No          | $50.00               | $5.00         |
| $50.00          | $5.00                    | $5.00    | No          | $45.00               | $0.00         |
| $65.00          | $0.00                    | $0.00    | No          | $65.00               | $5.00         |
| $2.00           | $5.00                    | $2.00    | No          | $0.00                | $0.00         |
| $5.00           | $3.00                    | $3.00    | No          | $2.00                | $0.00         |
| $283.00         | $0.00                    | $0.00    | Yes?        | $283.00?             | $5.00         |
| $100.00         | $5.00                    | $10.00   | Yes         | $90.00               | $5.00         |

