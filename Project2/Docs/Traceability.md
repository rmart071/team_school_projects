UC = Use Case


UC ID| UC Description| Requirements Description| Program Code | Design Element | Test Case IDs
:-----|:--------------------|:------------------------|:-------------|:---------------|:------------:
SCM 01 | Add Customer | Allow the manager to add customers to the system | addCustomer() | Use Cases/Class Design | 1.1, 1.2, 1.3
SCM 02 | Assign QR code to customer | Provide the customer with a QR code | addCustomer() | Use Cases/Class Design | 2.1, 2.2
SCM 03 | Scan QR Code | Allow the manager to scan the user's QR code | scanQR() | Use Cases/Class Design | 3.1, 3.2, 3.3
SCM 04 | Look up Customer | Allow the manager to look up customer information without QR code | accessCustomer() | Use Cases/Class Design | 4.1, 4.2, 4.3
SCM 05 | Edit Customer | Allow the manager to update customer information | editCustomer() | Use Cases/Class Design | 5.1, 5.2, 5.3
SCM 06 | Show History | Allow the manager to view all purchases and rewards made from the customer | listCustomerTransactions() | Use Cases/Class Design | 6.1, 6.2, 6.3
SCM 07 | Make Payment with scanner | Allow the manager to make transaction | processPurchases() | Use Cases/Class Design | 7.1, 7.2
SCM 08 | Make Payment with payment-processing service provider | Make Payment with payment-processing service provider | processPurchases() | Use Cases/Class Design | 8.1, 8.2
