# Design Document



**Author**: Team 36

## 1 Design Considerations

### 1.1 Assumptions

Software will be based on Android OS with baseline:
	
	Minimum Required SDK: API 19
	Target SDK: API 19 (KitKat)
	Compiled with: API 19 (KitKat)
	
	
Dependencies:

	Payment Processing Provider
	QR Reader
	Email Utility
	Internet/Cellular Access
	
Possible Project Issues:

	TBD when more information is available
	

	
	
### 1.2 Constraints

	Declined transactions are not recorded
	No ability to delete customers or purge/correct transactions
	Internet needed to send emails
	QR printer and reader needed (simulator provided)
	Sufficient Space to load Application and store Data
	Lookup method is not defined or required . Using Scan SQ Utility for lookup.
	



### 1.3 System Environment

	Android phone or tablet running OS 4.4.4 (KitKat) or greater
	Nexus 5 or similar pixel resolution
	Sufficient storage space for the database and application
	Internet Connection
	QR Reader or simulator
	QR Card or Label Printeror simulator
	Credit Card Reader or simulator
	Working Email account
	Working Payment Provider Account


## 2 Architectural Design


### 2.1 Component Diagram
![Team 36 Use Component Diagram](ComponentDiagram.png)



### 2.2 Deployment Diagram
This diagram will be completed when we have additional information on the Credit Card Scanner, QR Code scanner and printer, Email and Payment provider. 
![Team 36 Deployment Diagram](DeploymentDiagram.png)

## 3 Low-Level Design
The design uses three classes managing the business logic. 

Cart Manager
	Controls interactions with the user and manages the system including:
	
	Add/Edit Customers
	Add new sales (transactions) ,
	Manage rewards (??)
	Display a list of an individual customers transactions and their reqard balance
	Lookup a customer via their QR code or First Name, Last Name, and Email address
	
Customer ClassUserInterface

	The customer class contains the customers unique identifying information, their reward balance, and the date of expiration of that balance, as well as the gold Discount percentage off. The customer handles these activities:
	
	Manages and Calculates rewards
	Determines Gold Status 
	Communicates with the Email service for status notifications
	
Transaction Class

	The transaction class is further abstracted and keeps track of individual sales. In also interacts with the Payment Processor Service to determine whether to write a transaction record, or to notify the Cart Manager of a failure.

		

### 3.1 Class Diagram

![Team 36 Class Diagram](ClassDesign.png)

### 3.2 Other Diagrams
No additional diagrams will be provided due to the simplicity of the app and time constraints.

## 4 User Interface Design


![Team 36 User Interface Design](UserInterfaceDesignFinal.png)


