# User Manual


## Smoothie Cart Manager
The smoothie cart manager is an application that enables the user to accept payments for organic smoothies.This 
application allows ther users' customers to pay using a credit cart and reward their most loyalcustomer. In order 
to do this, the system creates a unique QR code and assigns it to each customer,which contains their information. 
Using this QR code, the user can keep track of the transaction their customer has made and check if they have 
achieved a 'Gold' Status. The user uses the system to add customers, edit customer information, process purchases, 
and keep track of purchases and rewards.


##1. How to Use the System
####1.1 Logging on
The system does not require any login functionality. Once opened from an Android device, the system presents the home
screen with several options that include:

* Scan QR
* New Sale
* Show Hist (short for history)
* Add Customer
* Edit Customer

The main screen also has a Customer Name field

####1.2 Add Customer
In order for a customer to be added to the system, the *Add Customer* button must be selected. The system will then
retrieve and display the add customer view. The user must enter the following information:

* First Name
* Last Name
* Street Address ,City, State, Zip Code
* Email Address.

Upon entering the data, press the save button and the customer will be added to the system.

####1.3 Edit Customer
On the main menu, the *Edit Customer* button is selected and a view similar to the add customer is displayed. The user
can now update the customer information and press the update button in order to save the changes.


####1.4 Scan QR
On the main menu, the *Scan QR* button is selected and they system will scan the QR code provided by the customer. The
system will then present the customer's information.

####1.5 Add New Sale
> In order for this to take place, the system must have the customer's information already loaded.

On the main menu, the *New Sale* button is selected. 
The system then shows the New Sale view where the information can be inputted. The user must enter the sale amount, which
the system will use to calcutae any reward or 'Gold' status discount available. Once the discounts, if any,  have been applied,
the system will then present the final total, where the customer can select the *Process* button.  The system will return a message indicating a process success and sale has been completed. If the customer has purchased a net of over $50, an email will be sent informing them, and if they have purchased over $500 in the calendar year, an email will be sent informing them of their gold status.

####1.7 Show History
> In order for this to take place, the system must have the customer's information already loaded via Lookup or Scan QRCode.

On the main menu, the *Show Hist* button has been selected and the system displays the history view. Here the user can now
see any transactions processed for the customer as well as any rewards available for the customer.