# Vision Document

**Author**: Ryan Fitzerald / Team 36

## 1 Introduction

The SmoothieCartManager shall be a system used to manage selling organic smoothies at a weekend cart. The system shall allow the owners, Brad and Janet, to charge customers and track rewards.

## 2 Business Needs/Requirements

Smoothie Cart owners need a way to reliably charge customers for purchases and credit them for rewards earned via purchases.

## 3 Product / Solution Overview

The product vision is to develop a simple and reliable mobile application that can reliably take credit card orders. Further the system must be able to track customers, their purchaes, and any rewards earned and spent over time.

## 4 Major Features (Optional)

Feature 1 - Customer Management

Feature 2 - Order Processing

Feature 3 - Rewards Calculation and Application 

## 5 Scope and Limitations

TBD