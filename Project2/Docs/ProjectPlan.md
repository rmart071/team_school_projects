# Project Plan


**Author**: Team 36

## 1 Introduction

An android based smoothie card manager designed to process payments and manage a loyalty reward system for a smoothie cart. It is a single user system. Customers receive a reward or discount or both, depending on their purchases.

## 2 Process Description

The unified software process will be used.

###2.1 Inception

####2.1.1.1 Create Software Design

	During this activity the development team will select the best features from the individual designs, then merge them to create an improved design expressed in a UML diagram.

####2.1.1.2 Entrance Criteria

	Completion of the individual designs.
	
####2.1.1.3 Exit Criteria

	Completion of the group design document.
	
####2.1.2.1 Create Project Plan

	During this activity the project manager will create the project plan and present it to the team for input and modification.
	
####2.1.2.2 Entrance Criteria

	Project authorization.
	
####2.1.2.2 Exit Criteria

	Completion of the initial project plan.
	
####2.1.3.1 Create Vision Document

	During this activity the development team will create the Vision document, which outlines the business needs, solutino overview, major features, and scope/limitations/constraint.
	
####2.1.3.2 Entrance Criteria

	Project Authorization.
	
####2.1.3.2 Exit Criteria

	Completion of the Vision document.
	
###2.2 Elaboration

####2.2.1.1 Create Use-case Model

	During this activity the development team will outline use cases.	
####2.2.1.2 Entrance Criteria

	Completed Requirements document, Vision Document.
	
####2.2.1.3 Exit Criteria

	Completion of the Use-case model document.
	
####2.2.2.1 Create Supplementary requirements

	During this activity the development team will outline any supplementary requirements including requirements that do not fit the use case model, and non-functional requirements
	.	
####2.2.2.2 Entrance Criteria

	Completed Vision document, Use-case model document.
	
####2.2.2.3 Exit Criteria

	Completion of the Supplementary requirements document(s).

####2.2.3.1 Create Supplementary requirements

	During this activity the development team will outline any supplementary requirements including requirements that do not fit the use case model, and non-functional requirements
	.	
####2.2.3.2 Entrance Criteria

	Completed Vision document, Use-case model document.
	
####2.2.3.2 Exit Criteria

	Completion of the Supplementary requirements document(s).

####2.2.3.1 Create Test Plan without results

	During this activity the development team will outline a preliminary test plan based on the current design.
	.	
####2.2.3.2 Entrance Criteria

	Completed Vision document, Use-case model document.
	
####2.2.3.2 Exit Criteria

	Completion of the test case document.

###2.3 Construction 

####2.3.1.1 Revise Inception and Elaboration Documents

	During this activity the development team will revise previously constructed documents as necessary.	
####2.3.1.2 Entrance Criteria

	Completed Inception and Elaboration Phase Documents.
	
####2.3.1.3 Exit Criteria

	Revision of those documents.
	
####2.3.2.1 Create Tracebility Matrix

	During this activity the development team will create a tracability matrix for a few of the use cases.	.	
####2.3.2.2 Entrance Criteria

	Completed and revised Use-case model document.
	
####2.3.2.3 Exit Criteria

	Completion of the Traceability document.

####2.3.3.1 Create Initial App Versions

	During this activity the development team will construct a initial app based on the documenation in Inception and Elaboration phases.	
####2.3.3.2 Entrance Criteria

	Completed Inception and Elaboration Phase Documents, traceability documents, and use-case model document revisions.
	
####2.3.3.3 Exit Criteria

	Completeion of the initial version of the app.
	
####2.3.4.1 Create User Manual

	During this activity the development team will construct a user manual for the initial app.	
####2.3.4.2 Entrance Criteria

	Completed initial app.
	
####2.3.4.3 Exit Criteria

	Completion of the User Manual.
	
###2.4 Transition

####2.4.1.1 Revise Earlier Documents

	During this activity the development team will revise previously constructed documents as necessary.	
####2.4.1.2 Entrance Criteria

	Completed Inception and Elaboration Phase Documents.
	
####2.4.1.3 Exit Criteria

	Revision of those documents.
	
####2.4.2.1 Create Final App version

	During this activity the development team will complete the final version of the app.	.	
####2.4.2.2 Entrance Criteria

	Completed and revised documents and initial app.
	
####2.4.2.3 Exit Criteria

	Completion of the final app.




			

## 3 Team


###3.1 - Team members
	     	
	     	Ryan Fitzgerald (RF)
	     	Austin Click (AC)
	     	Ricardo Martinez (RM)
	     	Keason Sanvordenker (KS)
    		Brad and Janet (BJ)
    		Professor/Graduate Assistants (PGA)
    	
###3.2 Roles 

####3.2.1 Subject Matter Expert (SME)
			Requirements are captured from the SME's. They understand both the functional requirements and any processes needed to fulfill those requirements.
			
####3.2.2 Functional Analyst (FA)
			Elicit clear and concise requirements from the SMEs.
			
####3.2.3 Solutions Architect (SA)
			Transforms the requirements created by the FA into architecture and design documents. 
			
####3.2.4 Development Manager (DM)
			Refines SA documentation, acts as filter and point of contact for developers.
	
####3.2.5 Developer (DV)
			Writes code, creates unit test case.  
	
####3.2.6 Quality Assurance Manager(QA)
			Assures quality of solution and adherence to requirements.
	
####3.2.7 Deployment (DP)
			Packages code and deploys it.
	
####3.2.8 Documentation/Training Lead (DL)
			Creates formal system documentation and training materials/classes/CBT for system. 
		
####3.2.9 Project Manager (PM)
			Assigns roles and resources, assesses and reports progress,  mitigates risk, manages finances, organizes project.
   
    
    
###3.3 Role Assignments
			
			Role			Lead		Other Group Members
			SME				PGA			Brad and Janet
			FA				Austin		Ryan, Keason, Ricardo
			SA				Ryan	
			DM				Ryan	
			DV				Ryan		Keason, Ricardo, Austin
			QA				Ricardo		Keason, Austin, Ryan
			DP				Ryan
			DL				Austin		Keason, Ricardo, Ryan
			PM				Keason		