# Team School Projects #

Worked with students to finish two projects.

### Project 1 ###

* System reads from a .txt file 
* Able to count number of sentences
* Able to count number of words by filtering out delimiters
* Set up JUnit Tests for functionalities

### Project 2 - Smoohtie Cart Manager###

* Created design documentation for system
* Able to create users
* View user's transaction history
* Process payments
* Created JUnit Test cases